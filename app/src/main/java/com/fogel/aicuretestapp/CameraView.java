package com.fogel.aicuretestapp;

import static com.fogel.aicuretestapp.Constants.DATE_FORMAT;
import static com.fogel.aicuretestapp.Constants.IMAGE_FOLDER;
import static com.fogel.aicuretestapp.Constants.VIDEO_FOLDER;

import android.content.Context;
import android.hardware.Camera;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.Date;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.FFmpegFrameFilter;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameFilter.Exception;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.JavaCameraView;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class CameraView extends JavaCameraView implements CvCameraViewListener2 {

    private onNewImageListener listener;

    private Mat colorRgba;

    private String mVideoFile = "";

    private boolean isRecord = false;

    private FFmpegFrameRecorder recorder;

    private long startTime = 0;

    private long videoTimestamp = 0;

    private int sampleAudioRateInHz = 44100;

    private Thread audioThread;

    private volatile boolean runAudioThread = true;

    private Frame yuvImage;

    public CameraView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setListener(onNewImageListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        colorRgba = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        colorRgba.release();
    }

    @Override
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        colorRgba = inputFrame.rgba();
        if (isRecord) {
            savePictures(colorRgba);
        }
        return colorRgba;
    }

    @Override
    public void onPreviewFrame(final byte[] frame, final Camera arg1) {
        super.onPreviewFrame(frame, arg1);
        if (yuvImage != null && isRecord) {
            videoTimestamp = 1000 * (System.currentTimeMillis() - startTime);
            ((ByteBuffer) yuvImage.image[0].position(0)).put(frame);
            try {
                recorder.setTimestamp(videoTimestamp);
                if (getCameraIndex() == CAMERA_ID_FRONT) {
                    FFmpegFrameFilter filter = new FFmpegFrameFilter("hflip", colorRgba.width(), colorRgba.height());
                    filter.setPixelFormat(avutil.AV_PIX_FMT_NV21);
                    filter.start();
                    filter.push(yuvImage);
                    Frame frame1;
                    while ((frame1 = filter.pull()) != null) {
                        recorder.record(frame1, avutil.AV_PIX_FMT_NV21);
                    }
                } else {
                    recorder.record(yuvImage);
                }

            } catch (FFmpegFrameRecorder.Exception e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void savePictures(Mat mat) {
        AsyncTask.execute(() -> {
            if (isRecord) {
                final Mat dest = mat.clone();
                if (getCameraIndex() == CameraBridgeViewBase.CAMERA_ID_FRONT) {
                    Core.rotate(dest, dest, Core.ROTATE_90_COUNTERCLOCKWISE);
                    Core.flip(dest, dest, 1);
                } else {
                    Core.rotate(mat, dest, Core.ROTATE_90_CLOCKWISE);
                }
                final String mImageFile = IMAGE_FOLDER + "/" + DATE_FORMAT.format(new Date()) + ".png";
                Imgproc.cvtColor(dest, dest, Imgproc.COLOR_RGB2BGR);
                Imgcodecs.imwrite(mImageFile, dest);
                listener.onNewImage(mImageFile);
            }
        });
    }

    private String getVideoFilePath() {
        File file = new File(VIDEO_FOLDER + "/" + DATE_FORMAT.format(new Date()) + ".mp4");
        return file.getAbsolutePath();
    }

    private void initRecorder() {
        videoTimestamp = 0;
        yuvImage = new Frame(colorRgba.width(), colorRgba.height(), Frame.DEPTH_UBYTE, 2);
        recorder = new FFmpegFrameRecorder(mVideoFile, colorRgba.width(), colorRgba.height(), 1);
        recorder.setFormat("mp4");
        recorder.setSampleRate(sampleAudioRateInHz);
        recorder.setFrameRate(25);
        if (getCameraIndex() == CAMERA_ID_BACK) {
            recorder.setVideoMetadata("rotate", "90");
        } else {
            recorder.setVideoMetadata("rotate", "90");
        }

        final AudioRecordRunnable audioRecordRunnable = new AudioRecordRunnable();
        audioThread = new Thread(audioRecordRunnable);
    }

    public void startRecord() {
        createFolders();
        mVideoFile = getVideoFilePath();
        initRecorder();
        try {
            recorder.start();
            startTime = System.currentTimeMillis();
            audioThread.start();
            isRecord = true;
        } catch (FFmpegFrameRecorder.Exception e) {
            e.printStackTrace();
        }
    }

    public void stopRecord() {
        isRecord = false;
        runAudioThread = false;
        if (recorder != null) {
            try {
                recorder.stop();
                recorder.release();
            } catch (FFmpegFrameRecorder.Exception e) {
                e.printStackTrace();
            }
            recorder = null;
        }

    }

    private void init() {
        setVisibility(SurfaceView.VISIBLE);
        setCameraIndex(CameraBridgeViewBase.CAMERA_ID_BACK);
        setCvCameraViewListener(this);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void createFolders() {
        File imagePath = new File(IMAGE_FOLDER);
        imagePath.mkdirs();
        File videoPath = new File(VIDEO_FOLDER);
        videoPath.mkdirs();
    }

    public boolean isRecord() {
        return isRecord;
    }

    class AudioRecordRunnable implements Runnable {

        @Override
        public void run() {
            runAudioThread = true;
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            int bufferSize;
            short[] audioData;
            int bufferReadResult;
            bufferSize = AudioRecord.getMinBufferSize(sampleAudioRateInHz, AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);
            AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleAudioRateInHz,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
            audioData = new short[bufferSize];
            audioRecord.startRecording();
            while (runAudioThread) {
                bufferReadResult = audioRecord.read(audioData, 0, audioData.length);
                if (bufferReadResult > 0) {
                    if (isRecord) {
                        try {
                            recorder.recordSamples(ShortBuffer.wrap(audioData, 0, bufferReadResult));
                        } catch (FFmpegFrameRecorder.Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            audioRecord.stop();
            audioRecord.release();
        }
    }


    public interface onNewImageListener {

        void onNewImage(String string);
    }
}
