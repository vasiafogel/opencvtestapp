package com.fogel.aicuretestapp;


import static com.fogel.aicuretestapp.Constants.IMAGE_FOLDER;

import android.Manifest.permission;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.fogel.aicuretestapp.CameraView.onNewImageListener;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;

public class MainActivity extends AppCompatActivity implements onNewImageListener {

    public static final int REQUEST_PERMISSION = 1;

    private CameraView cameraView;
    private ImageView ivChangeCamera;
    private ImageView ivRecord;
    private TextView tvSaveFolder;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(final int status) {
            switch (status) {
                case BaseLoaderCallback.SUCCESS:
                    cameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.loadLibrary("opencv_java3");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        initViews();
        initClickListeners();
        checkForPermissions();
    }

    @Override
    public void onNewImage(final String string) {
        runOnUiThread(() -> {
            if(cameraView.isRecord()) {
                tvSaveFolder.setText(string);
            } else {
                String imgFolder = "Save Folder: " + IMAGE_FOLDER;
                tvSaveFolder.setText(imgFolder);
            }
        });
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")


    private void initViews() {
        cameraView = findViewById(R.id.camera_view);
        tvSaveFolder = findViewById(R.id.tv_save_folder);
        ivChangeCamera = findViewById(R.id.iv_change_camera);
        ivRecord = findViewById(R.id.iv_record_video);
        String imgFolder = "Save Folder: " + IMAGE_FOLDER;
        tvSaveFolder.setText(imgFolder);
        cameraView.setListener(this);
    }

    private void initClickListeners() {
        ivChangeCamera.setOnClickListener(v -> {
            Animation aniRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
            ivChangeCamera.startAnimation(aniRotate);
            if (cameraView.getCameraIndex() == CameraBridgeViewBase.CAMERA_ID_BACK) {
                cameraView.disableView();
                cameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_FRONT);
                cameraView.enableView();
            } else {
                cameraView.disableView();
                cameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_BACK);
                cameraView.enableView();
            }
        });
        ivRecord.setOnClickListener(v -> {
            if (!cameraView.isRecord()) {
                ivChangeCamera.setVisibility(View.GONE);
                ivRecord.setBackground(getResources().getDrawable(R.drawable.ic_stop));
                cameraView.startRecord();
            } else {
                cameraView.stopRecord();
                ivRecord.setBackground(getResources().getDrawable(R.drawable.ic_record));
                ivChangeCamera.setVisibility(View.VISIBLE);
                String imgFolder = "Save Folder: " + IMAGE_FOLDER;
                tvSaveFolder.setText(imgFolder);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isPermissionGranted()) {
            if (OpenCVLoader.initDebug()) {
                mLoaderCallback.onManagerConnected(BaseLoaderCallback.SUCCESS);
            } else {
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
            }
            ivRecord.setBackground(getResources().getDrawable(R.drawable.ic_record));
            ivChangeCamera.setVisibility(View.VISIBLE);
            String imgFolder = "Save Folder: " + IMAGE_FOLDER;
            tvSaveFolder.setText(imgFolder);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.stopRecord();
        if (cameraView != null) {
            cameraView.disableView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cameraView != null) {
            cameraView.disableView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
            @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    onResume();
                } else {
                    AlertDialog.Builder builder = new Builder(this);
                    builder.setTitle("Permissions access required");
                    builder.setMessage(
                            "App require user to allow camera permission to be able use the application, would you like to grant camera permissions?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", (dialog, which) -> checkForPermissions());
                    builder.setNegativeButton("No", ((dialog, which) -> finish()));
                    builder.show();
                }
                break;
        }
    }

    private boolean isPermissionGranted(){
        if (ContextCompat.checkSelfPermission(this, permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private void checkForPermissions(){
        ActivityCompat.requestPermissions(this, new String[]{
                        permission.CAMERA,
                        permission.WRITE_EXTERNAL_STORAGE,
                        permission.READ_EXTERNAL_STORAGE,
                        permission.RECORD_AUDIO},
                REQUEST_PERMISSION);
    }
}
