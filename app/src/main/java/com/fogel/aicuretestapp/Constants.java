package com.fogel.aicuretestapp;

import android.os.Environment;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class  Constants {
    public static final String ROOT_FOLDER = Environment.getExternalStorageDirectory() + "/AICure";
    public static final String IMAGE_FOLDER = ROOT_FOLDER + "/Image";
    public static final String VIDEO_FOLDER = ROOT_FOLDER + "/Video";

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss:SS", Locale.US);

}
